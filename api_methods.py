""" Connector to BibTexAPI """

from bibtex.apis.api_base import get_prepared_title
from bibtex.apis.scholar import get_number_of_cites as get_number_of_cites_scholar
from bibtex.apis.openalex import get_number_of_cites as get_number_of_cites_alex


def get_number_of_cites(title_str: str, api_method: str = 'OpenAlex') -> int:

    prepared_title = get_prepared_title(title_str)

    # noinspection PyBroadException
    try:
        if api_method == 'OpenAlex':
            return get_number_of_cites_alex(prepared_title)
        elif api_method == 'GoogleScholar':
            return get_number_of_cites_scholar(prepared_title)
        else:
            raise Exception('API "{}" is unknown.'.format(api_method))
    except Exception as err:
        if err.args[0] == 'No Result':
            return -1
        else:
            raise err

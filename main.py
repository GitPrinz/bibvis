""" Host a Bokeh Server to browse BibTex database

Will open a loading window on startup.

"""

from tqdm import tqdm

from bokeh.plotting import curdoc
from bokeh.layouts import layout
from bokeh.models import ColumnDataSource, Select, Button

from bibtex.filtered import BibtexFiltered

from bokeh_scripts.plot_table import TablePlot
from bokeh_scripts.plot_vs import VSPlot
from bokeh_scripts.plot_ranking import RankingPlot

from api_methods import get_number_of_cites


# loading a File
import tkinter as tk
from tkinter import filedialog

##############
# Parameters #
##############

window_width = 1024


################
# Data Loading #
################

citations_column_name = '#Citations'

# don't show tk window.
root = tk.Tk()
root.withdraw()

file_path = filedialog.askopenfilename()
bibob = BibtexFiltered(file_path)
# a default path would be good for testing: "BibtexVisualization/bibtex/demo.bib"


####################
# Data Preparation #
####################


bibtex_source = ColumnDataSource(bibob.get_dataframe_filtered().rename(columns={'ID': 'id', 'year': 'Year'}))
# todo: deal with "index" and "id"
bibtex_source.data[citations_column_name] = ['unknown'] * len(bibtex_source.data['index'])


# list columns (value or label) used for widgets and tables
value_columns = ["volume", "number", "Year", citations_column_name]
vco = value_columns
label_columns = list(set(bibtex_source.column_names) - set(value_columns))
lco = label_columns


def get_selected_data(label: str) -> list:
    """
    easy access to (selected) samples
    :param label: column name
    :return: list of values
    """
    if len(bibtex_source.selected.indices) > 0:
        return bibtex_source.data[label][bibtex_source.selected.indices]
    else:
        # noinspection PyTypeChecker
        return bibtex_source.data[label]
# todo: selection doesn't work!?

#################
# Bokeh Widgets #
#################


# Table #
#########

def get_table_columns() -> list:
    """Control format of columns. Mismatch in actual Format will be **shown** as NaN"""
    # gather all columns but remove columns with special layout
    lcos = [(column, {'formatter': str}) for column in lco]
    vcos = [(column, {'formatter': float}) for column in vco
            if column not in ['Year']]

    # noinspection PyTypeChecker
    return [('Year', 'year')] + lcos + vcos


p_tab = TablePlot(source=bibtex_source, get_columns=get_table_columns, width=window_width)
p_tab.title = '<b>Bibtex Database</b>'

p_meta_switch = Select(title='Meta source', value='OpenAlex', options=['OpenAlex', 'GoogleScholar'])
p_meta_citations_button = Button(label='Load Citations')


def load_citations_cb(_event) -> None:
    """ Update source with column with number of citations """

    for ii in tqdm(range(len(bibtex_source.data['index'])), 'Checking sources: '):
        bibtex_source.patch({citations_column_name: [
            (ii, get_number_of_cites(title_str=bibtex_source.data['title'][ii],
                                     api_method=p_meta_switch.value))]})

    return


p_meta_citations_button.on_click(load_citations_cb)


# VS #
######

p_vs = VSPlot(get_selected_data,
              options_x=value_columns + label_columns,
              options_y=value_columns + label_columns,
              options_size=value_columns + label_columns,
              options_color=label_columns + value_columns,
              option_default_x='Year', option_default_y='Year', option_default_color='journal')
p_vs.fig.height = 768
p_vs.fig.width = window_width

# Ranking #
############

p_ranking = RankingPlot(get_selected_data,
                        options=label_columns,
                        option_default='journal')

p_ranking.fig.height = 384
p_ranking.fig.width = window_width


################
# Bokeh Server #
################

# updates #
###########

def update_selection_cb(_attr, _old, _new) -> None:
    p_vs.update()
    p_ranking.update()
    return


bibtex_source.selected.on_change('indices', update_selection_cb)
bibtex_source.on_change('data', update_selection_cb)


# Serve #
#########

doc = curdoc()
doc.title = "Sample Overview"

# noinspection PyTypeChecker
doc.add_root(layout([
    [[p_tab.layout], [p_meta_switch, p_meta_citations_button]],
    p_ranking.layout,
    p_vs.layout
]))

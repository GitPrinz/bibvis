# Bibtex Visualization

Some simple Python scripts for my needs.
**Be aware everything can and probably will change someday.**

Interface to load any Bibtex file with
- Table
- VS-Plot
- Ranking

## Idea

Visualize content of a Bibtex File.

- Different Publications vs. Year
- Connection between Authors

Example of Interface:

![interface_example](interface_example.png)

## Setup Notes

Add the Submodules as "Source root"

### Run Bokeh in PyCharm

Run/Debug Configuration:
- ScriptPath / >>Module Name<<: bokeh
- Parameters: serve BibtexVisualization
- Working Directory: "Top Level Folder"

For requirements: check submodules.

## Todo

- [x] Add Bibtex Selection
- [ ] Improve Interface?
- [ ] Improve Visualization of Strings